output "lb_ip_address" {
  value = yandex_lb_network_load_balancer.lb-rai.*
}

output "default_instance_public_ip" {
    value = [yandex_compute_instance.vm-1.network_interface[0].nat_ip_address,
             yandex_compute_instance.vm-2.network_interface[0].nat_ip_address,
             yandex_compute_instance.vm-3.network_interface[0].nat_ip_address]
}