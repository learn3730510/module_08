terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token = "y0_AgAAAAAMBQS7AATuwQAAAADf0uwNHgvmHmr4RpuxS9InFbKKk3j-RO0"
  cloud_id  = "b1gp8lr5vq4et15ermu8"
  folder_id = "b1gq441l9n1pl3gc9n47"
  zone      = "ru-central1-a"
}

data "yandex_compute_image" "last_ubuntu" {
  family = "ubuntu-2204-lts"  # ОС (Ubuntu, 22.04 LTS)
}

data "yandex_vpc_subnet" "default_a" {
  name = "default-ru-central1-a"  # одна из дефолтных подсетей
}
